package com.rentalcar.webapi.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserDto {
  private Integer id;

  @JsonProperty("name")
  private String name;

  @JsonProperty("email")
  private String email;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime created_at;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime updated_at;

  private List<Integer> rentalsOwnedIds;
  private List<Integer> rentalsRentIds = new ArrayList<>();
  private int roleId;

  // Constructors, if needed

  // Getters and Setters

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public LocalDateTime getCreated_at() {
    return created_at;
  }

  public void setCreated_at(LocalDateTime created_at) {
    this.created_at = created_at;
  }

  public LocalDateTime getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(LocalDateTime updated_at) {
    this.updated_at = updated_at;
  }

  public List<Integer> getRentalsOwnedIds() {
    return rentalsOwnedIds;
  }

  public void setRentalsOwnedIds(List<Integer> rentalsOwnedIds) {
    this.rentalsOwnedIds = rentalsOwnedIds;
  }

  public List<Integer> getRentalsRentIds() {
    return rentalsRentIds;
  }

  public void setRentalsRentIds(List<Integer> rentalsRentIds) {
    this.rentalsRentIds = rentalsRentIds;
  }

  public int getRoleId() {
    return roleId;
  }

  public void setRoleId(int roleId) {
    this.roleId = roleId;
  }
}
