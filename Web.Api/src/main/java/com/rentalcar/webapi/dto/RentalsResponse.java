package com.rentalcar.webapi.dto;

import java.util.List;

public class RentalsResponse {
  private List<PropertyDto> rentals;

  public List<PropertyDto> getRentals(){return rentals;}

  public void setRentals(List<PropertyDto> rentals){this.rentals = rentals;}

}
