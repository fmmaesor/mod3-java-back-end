package com.rentalcar.webapi.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImageService {

  private static final Logger logger = LoggerFactory.getLogger(ImageService.class);

  @Value("${external.images.folder}")
  private String imageDirectory;

  public byte[] getImageData(String imageName) {
    try {
      // Construct the path to the image
      Path imagePath = Paths.get(imageDirectory, imageName);

      // Read the image data into a byte array
      return Files.readAllBytes(imagePath);
    } catch (IOException e) {
      // Handle exceptions, e.g., log or throw a custom exception
      logger.error("Error reading image data for {}: {}", imageName, e.getMessage());
      return null; // Or throw an exception based on your error handling strategy
    }
  }

  public void saveFileToExternalFolder(MultipartFile file) throws IOException {
    String fileName = file.getOriginalFilename();
    byte[] bytes = file.getBytes();
    String filePath = Paths.get(imageDirectory, fileName).toString();
    File outputFile = new File(filePath);
    try (FileOutputStream fos = new FileOutputStream(outputFile)) {
      fos.write(bytes);
    }
  }

}

