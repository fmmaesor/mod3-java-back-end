package com.rentalcar.webapi.services;

import com.rentalcar.webapi.domain.Role;
import com.rentalcar.webapi.domain.User;
import com.rentalcar.webapi.dto.UserFormDataDto;
import com.rentalcar.webapi.dto.UserDto;
import com.rentalcar.webapi.mapper.UserMapper;
import com.rentalcar.webapi.repository.RoleRepository;
import com.rentalcar.webapi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;

@Service
@Transactional
public class UserService {

  private final UserRepository userRepository;
  private final UserMapper userMapper;
  private final PasswordEncoder passwordEncoder;
  private final RoleRepository roleRepository;

  @Autowired
  public UserService(UserRepository userRepository, UserMapper userMapper, PasswordEncoder passwordEncoder, RoleRepository roleRepository) {
    this.userRepository = userRepository;
    this.userMapper = userMapper;
    this.passwordEncoder = passwordEncoder;
    this.roleRepository = roleRepository;
  }

  @Transactional(readOnly = true)
  public UserDto getFromId(Integer id) {
    return userRepository.findById(id)
      .map(userMapper::toDto)
      .orElse(null);
  }

  public UserDto createUser(UserFormDataDto user) {
    UserDto userDto = new UserDto();
    userDto.setName(user.getName());
    userDto.setEmail(user.getEmail());
    userDto.setCreated_at(LocalDateTime.now());
    userDto.setUpdated_at(LocalDateTime.now());
    User userEntity = userMapper.toEntity(userDto);
    userEntity.setPassword(this.passwordEncoder.encode(user.getPassword()));
    userEntity.setRentalsOwned(new ArrayList<>());

    Role role = roleRepository.findById(1)
      .orElseThrow(() -> new RuntimeException("Role not found"));

    userEntity.setRole(role);    User savedUser = userRepository.save(userEntity);
    return userMapper.toDto(savedUser);
  }

  @Transactional(readOnly = true)
  public Optional<UserDto> getUserFromName(String name) {
    return userRepository.findByName(name).map(userMapper::toDto);
  }

  @Transactional(readOnly = true)
  public Optional<UserDto> getUserFromEmail(String email) {
    return userRepository.findByEmail(email).map(userMapper::toDto);
  }

  public Optional<UserDto> updateUser(Integer userId, String newName, String newPassword, String newEmail) {
    return userRepository.findById(userId)
      .map(user -> {
        if (!"".equals(newName)) {
          user.setUserName(newName);
        }

        if (!"".equals(newPassword)) {
          user.setPassword(this.passwordEncoder.encode(newPassword));
        }

        if (!"".equals(newEmail)) {
          user.setEmail(newEmail);
        }

        user.setUpdatedAt(LocalDateTime.now()); // Assuming updatedAt is managed by the database
        return userMapper.toDto(userRepository.save(user));
      });
  }

  public boolean deleteUser(Integer userId) {
    Optional<User> existingUser = userRepository.findById(userId);

    if (existingUser.isPresent()) {
      userRepository.delete(existingUser.get());
      return true;
    }
    return false; // User not found
  }


}
