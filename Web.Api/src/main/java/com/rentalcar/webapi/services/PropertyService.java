package com.rentalcar.webapi.services;

import com.rentalcar.webapi.domain.Property;
import com.rentalcar.webapi.dto.PropertyDto;
import com.rentalcar.webapi.mapper.PropertyMapper;
import com.rentalcar.webapi.repository.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
public class PropertyService {

  private final PropertyRepository propertyRepository;
  private final PropertyMapper propertyMapper;

  private final ImageService imageService;

  @Autowired
  public PropertyService(PropertyRepository propertyRepository, PropertyMapper propertyMapper, ImageService imageService) {
    this.propertyRepository = propertyRepository;
    this.propertyMapper = propertyMapper;
    this.imageService = imageService;
  }

  @Transactional(readOnly = true)
  public List<PropertyDto> getAllProperties() {
    List<Property> properties = propertyRepository.findAll();
    for (Property property: properties) {
      getPropertyImage(property);
    }
    return properties.stream().map(propertyMapper::toDto).toList();
  }


  @Transactional(readOnly = true)
  public PropertyDto getPropertyById(Integer id) {
    Optional<Property> property = propertyRepository.findById(id);
    Property extractedProperty = property.orElse(new Property());
    getPropertyImage(extractedProperty);
    return property.map(propertyMapper::toDto).orElse(null);
  }

  @Transactional(readOnly = true)
  public Optional<PropertyDto> getPropertyByName(String name) {
    return propertyRepository.findByName(name).map(propertyMapper::toDto);
  }

  public List<Property> getPropertiesByUserId(int userId) {
    return propertyRepository.findByOwner_Id(userId);
  }

  public PropertyDto createProperty(PropertyDto propertyDto, Integer ownerId) {
    Property property = propertyMapper.toEntityWithCurrentUser(propertyDto, ownerId);
    Property savedProperty = propertyRepository.save(property);
    return propertyMapper.toDto(savedProperty);
  }

  public PropertyDto updateProperty(Integer id, PropertyDto updatedPropertyDto) {
    Optional<Property> existingPropertyOptional = propertyRepository.findById(id);

    if (existingPropertyOptional.isPresent()) {
      Property existingProperty = existingPropertyOptional.get();
      Property updatedProperty = propertyMapper.toEntity(updatedPropertyDto);
      existingProperty.setSurface(updatedProperty.getSurface());
      existingProperty.setPrice(updatedProperty.getPrice());
      existingProperty.setPicture(updatedProperty.getPicture());
      existingProperty.setDescription(updatedProperty.getDescription());
      existingProperty.setOwner(updatedProperty.getOwner());
      existingProperty.setUpdatedAt(LocalDateTime.now()); // Assuming updatedAt is managed by the database

      return propertyMapper.toDto(propertyRepository.save(existingProperty));
    }

    return null; // Property not found
  }

  public boolean deleteProperty(Integer propertyId) {
    Optional<Property> existingProperty = propertyRepository.findById(propertyId);

    if (existingProperty.isPresent()) {
      propertyRepository.delete(existingProperty.get());
      return true;
    }
    return false;
  }

  private void getPropertyImage(Property property) {
    String imageName = property.getPicture();
    if (imageName != null) {
      byte[] imageData = imageService.getImageData(imageName);
      if (imageData != null) {
        String base64Image = Base64.getEncoder().encodeToString(imageData);
        property.setPicture("data:image/jpeg;base64," + base64Image);
      }
    }
  }
}
