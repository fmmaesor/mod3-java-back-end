package com.rentalcar.webapi.services;

import com.rentalcar.webapi.domain.Message;
import com.rentalcar.webapi.dto.MessageDto;
import com.rentalcar.webapi.mapper.MessageMapper;
import com.rentalcar.webapi.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Transactional
public class MessageService {

  private final MessageRepository messageRepository;
  private final MessageMapper messageMapper;

  @Autowired
  public MessageService(MessageRepository messageRepository, MessageMapper messageMapper) {
    this.messageRepository = messageRepository;
    this.messageMapper = messageMapper;
  }

  @Transactional(readOnly = true)
  public MessageDto getMessageById(Integer messageId) {
    return messageRepository.findById(messageId)
      .map(messageMapper::toDto)
      .orElse(null);
  }

  public MessageDto createMessage(MessageDto messageDto) {
    Message message = messageMapper.toEntity(messageDto);
    return messageMapper.toDto(messageRepository.save(message));
  }

  @Transactional(readOnly = true)
  public Optional<MessageDto> getMessageByPropertyId(Integer propertyId) {
    return messageRepository.findByPropertyId(propertyId).map(messageMapper::toDto);
  }

  public Optional<MessageDto> updateMessage(Integer messageId, String newMessage) {
    return messageRepository.findById(messageId)
      .map(message -> {
        message.setMessage(newMessage);
        message.setCreatedAt(LocalDateTime.now()); // Assuming createdAt is managed by the database
        return messageMapper.toDto(messageRepository.save(message));
      });
  }

  public boolean deleteMessage(Integer messageId) {
    Optional<Message> existingMessage = messageRepository.findById(messageId);

    if (existingMessage.isPresent()) {
      messageRepository.delete(existingMessage.get());
      return true;
    }

    return false; // Message not found
  }
}
