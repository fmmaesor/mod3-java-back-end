package com.rentalcar.webapi.repository;

import com.rentalcar.webapi.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {

  Optional<Role> findById(Integer id);
}
