package com.rentalcar.webapi.repository;

import com.rentalcar.webapi.domain.Property;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PropertyRepository extends JpaRepository<Property, Integer> {
  List<Property> findByOwner_Id(Integer userId);

  Optional<Property> findByName(String name);
}
