package com.rentalcar.webapi.repository;

import com.rentalcar.webapi.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MessageRepository extends JpaRepository<Message, Integer> {

  Optional<Message> findById(int id);

  Optional<Message> findByPropertyId(Integer propertyId);

  // You can add more custom queries if needed
}
