package com.rentalcar.webapi.security.configuration;

import com.rentalcar.webapi.security.JwtTokenProvider;
import com.rentalcar.webapi.security.utils.filters.JwtFilter;
import com.rentalcar.webapi.services.CustomUserDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private CustomUserDetailsService customUserDetailsService;

  private JwtFilter jwtFilter;

  private JwtTokenProvider jwtTokenProvider;

  private String webAppUrl;

  @Autowired
  public void setCustomUserDetailsService(CustomUserDetailsService customUserDetailsService) {
    this.customUserDetailsService = customUserDetailsService;
  }

  @Autowired
  public SecurityConfig(JwtFilter jwtFilter, JwtTokenProvider jwtTokenProvider, @Value("${webapp.url}") String webAppUrl){
    this.jwtFilter = jwtFilter;
    this.jwtTokenProvider = jwtTokenProvider;
    this.webAppUrl = webAppUrl;
  }


   @Override
  protected void configure(HttpSecurity http) throws Exception {
     http
       .cors()
       .and()
       .addFilterBefore(new CorsFilter(corsConfigurationSource()), UsernamePasswordAuthenticationFilter.class)
       .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class)
        .exceptionHandling()
        .and()
          .csrf()
          .disable()
        .headers()
        .frameOptions()
          .disable()
        .and()
          .sessionManagement()
          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
          .authorizeRequests()
              .antMatchers("/api/auth/login").permitAll() // Allow access to the login endpoint without authentication
              .antMatchers("/api/auth/register").permitAll() // Allow access to the register endpoint without authentication
              .antMatchers("/swagger-ui.html", "/swagger-ui/**").permitAll() // Allow access to Swagger UI without authentication
              .antMatchers("/api/**").authenticated() // Require authentication for accessing other endpoints
              .antMatchers("/admin").hasRole("ADMIN")
              .antMatchers("/user").hasRole("USER")
        .and()
          .apply(securityConfigurerAdapter());
  }

  public void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(customUserDetailsService).passwordEncoder(bCryptPasswordEncoder());
  }

  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.addAllowedOrigin(webAppUrl);
    configuration.addAllowedMethod("*");
    configuration.addAllowedHeader("*");

    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/api/**", configuration);
    return source;
  }

  private JwtConfigurer securityConfigurerAdapter() {
    return new JwtConfigurer(jwtTokenProvider);
  }

  @Override
  @Bean
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public PasswordEncoder bCryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
