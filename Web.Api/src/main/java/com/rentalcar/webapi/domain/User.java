package com.rentalcar.webapi.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "user")
public class User implements UserDetails {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "name")
  private String name;

  @Column(name = "password")
  private String password;

  @Column(name = "email")
  private String email;

  @Column(name = "created_at")
  private LocalDateTime createdAt;

  @Column(name = "updated_at")
  private LocalDateTime updatedAt;

  @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL)
  private List<Property> rentalsOwned;

  @ManyToOne(cascade = CascadeType.PERSIST)
  @JoinColumn(name = "role_id")
  private Role role;

  @Transient
  private Collection<? extends GrantedAuthority> authorities;

  public User() {
  }

  public User(String name, String password, String email, LocalDateTime createdAt, LocalDateTime updatedAt, Role role) {
    this.name = name;
    this.password = password;
    this.email = email;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.role = role;
  }

  public User(String username, String password, Collection<? extends GrantedAuthority> authorities) {
    this.name = username; // Assuming 'name' is equivalent to 'username'
    this.password = password;
    this.authorities = authorities;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    // Assuming you have a Role entity with a 'name' property
    return List.of(new SimpleGrantedAuthority(role.getName()));
  }

  @Override
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String getUsername() {
    return name; // Assuming email is the unique identifier for your users
  }

  @Override
  public boolean isAccountNonExpired() {
    return true; // You can implement additional logic if needed
  }

  @Override
  public boolean isAccountNonLocked() {
    return true; // You can implement additional logic if needed
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true; // You can implement additional logic if needed
  }

  @Override
  public boolean isEnabled() {
    return true; // You can implement additional logic if needed
  }

  // Generate getters and setters manually
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setUserName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public LocalDateTime getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(LocalDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  public List<Property> getRentalsOwned() {
    return rentalsOwned == null ? new ArrayList<>() : rentalsOwned;
  }

  public void setRentalsOwned(List<Property> rentalsOwned) {
    this.rentalsOwned = rentalsOwned;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public void setRentalsRent(List<Property> rentedProperties) {

  }
}
