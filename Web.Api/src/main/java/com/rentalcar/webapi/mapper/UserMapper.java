package com.rentalcar.webapi.mapper;

import com.rentalcar.webapi.domain.Property;
import com.rentalcar.webapi.domain.Role;
import com.rentalcar.webapi.domain.User;
import com.rentalcar.webapi.dto.UserDto;
import com.rentalcar.webapi.services.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class UserMapper {

  @Autowired
  private PropertyService propertyService;

  @Autowired
  private PropertyMapper propertyMapper;

  public UserDto toDto(User user) {
    UserDto userDto = new UserDto();
    userDto.setId(user.getId());
    userDto.setName(user.getName());
    userDto.setEmail(user.getEmail());
    userDto.setCreated_at(user.getCreatedAt());
    userDto.setUpdated_at(user.getUpdatedAt());
    userDto.setRentalsOwnedIds(user.getRentalsOwned().stream().map(Property::getId).collect(Collectors.toList()));
    if (user.getRole() != null) {
      userDto.setRoleId( user.getRole().getId());
    }
    return userDto;
  }
  public User toEntity(UserDto userDto) {
    User user = new User();
    user.setId(userDto.getId());
    user.setUserName(userDto.getName());
    user.setEmail(userDto.getEmail());
    user.setCreatedAt(userDto.getCreated_at());
    user.setUpdatedAt(userDto.getUpdated_at());

    Role role = new Role();
    role.setId(userDto.getRoleId());  // Assuming Role has a setId method
    user.setRole(role);


    return user;
  }

}
