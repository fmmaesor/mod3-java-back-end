package com.rentalcar.webapi.mapper;

import com.rentalcar.webapi.domain.Property;
import com.rentalcar.webapi.domain.User;
import com.rentalcar.webapi.dto.PropertyDto;
import org.springframework.stereotype.Component;

@Component
public class PropertyMapper {

  public PropertyDto toDto(Property property) {
    PropertyDto propertyDTO = new PropertyDto();
    propertyDTO.setId(property.getId());
    propertyDTO.setName(property.getName());
    propertyDTO.setSurface(property.getSurface());
    propertyDTO.setPrice(property.getPrice());
    propertyDTO.setPicture(property.getPicture());
    propertyDTO.setDescription(property.getDescription());
    propertyDTO.setCreated_at(property.getCreatedAt());
    propertyDTO.setUpdated_at(property.getUpdatedAt());
    if (property.getOwner() != null) {
      propertyDTO.setOwner_id(property.getOwner().getId());
    }
    return propertyDTO;
  }

  public Property toEntityWithCurrentUser(PropertyDto propertyDto, Integer ownerId) {
    Property property = new Property();
    property.setId(propertyDto.getId());
    property.setName(propertyDto.getName());
    property.setSurface(propertyDto.getSurface());
    property.setPrice(propertyDto.getPrice());
    property.setPicture(propertyDto.getPicture());
    property.setDescription(propertyDto.getDescription());
    property.setCreatedAt(propertyDto.getCreated_at());
    property.setUpdatedAt(propertyDto.getUpdated_at());
    // Assuming you have a service method to fetch the User based on ID
    if (ownerId != null) {
      User owner = new User();
      owner.setId(ownerId);
      property.setOwner(owner);
    }
    return property;
  }

  public Property toEntity(PropertyDto propertyDto) {
    Property property = new Property();
    property.setId(propertyDto.getId());
    property.setName(propertyDto.getName());
    property.setSurface(propertyDto.getSurface());
    property.setPrice(propertyDto.getPrice());
    property.setPicture(propertyDto.getPicture());
    property.setDescription(propertyDto.getDescription());
    property.setCreatedAt(propertyDto.getCreated_at());
    property.setUpdatedAt(propertyDto.getUpdated_at());
    if (propertyDto.getOwner_id() != null) {
      User owner = new User();
      owner.setId(propertyDto.getOwner_id());
      property.setOwner(owner);
    }
    return property;
  }

}
