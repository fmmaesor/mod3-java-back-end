package com.rentalcar.webapi.mapper;

import com.rentalcar.webapi.domain.Message;
import com.rentalcar.webapi.dto.MessageDto;
import org.springframework.stereotype.Component;

@Component
public class MessageMapper {

  public MessageDto toDto(Message message) {
    MessageDto messageDto = new MessageDto();
    messageDto.setId(message.getId());
    messageDto.setMessage(message.getMessage());
    messageDto.setPropertyId(message.getPropertyId());
    messageDto.setUserId(message.getUserId());
    messageDto.setCreatedAt(message.getCreatedAt());
    return messageDto;
  }

  public Message toEntity(MessageDto messageDto) {
    Message message = new Message();
    message.setId(messageDto.getId());
    message.setMessage(messageDto.getMessage());
    message.setPropertyId(messageDto.getPropertyId());
    message.setUserId(messageDto.getUserId());
    message.setCreatedAt(messageDto.getCreatedAt());
    return message;
  }
}
