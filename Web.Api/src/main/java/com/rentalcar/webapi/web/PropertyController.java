package com.rentalcar.webapi.web;

import com.rentalcar.webapi.dto.PropertyDto;
import com.rentalcar.webapi.dto.RentalsResponse;
import com.rentalcar.webapi.dto.UserDto;
import com.rentalcar.webapi.services.ImageService;
import com.rentalcar.webapi.services.PropertyService;
import com.rentalcar.webapi.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/rentals")
public class PropertyController {

  private final PropertyService propertyService;
  private final UserService userService;
  private final ImageService imageService;

  @Autowired
  public PropertyController(PropertyService propertyService, UserService userService, ImageService imageService) {
    this.propertyService = propertyService;
    this.userService = userService;
    this.imageService = imageService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<PropertyDto> getPropertyById(@PathVariable Integer id) {
    PropertyDto propertyDto = propertyService.getPropertyById(id);
    return propertyDto != null
      ? ResponseEntity.ok(propertyDto)
      : ResponseEntity.notFound().build();
  }

  @GetMapping
  public ResponseEntity<RentalsResponse> getAllProperties() {
    List<PropertyDto> propertyDtoList = propertyService.getAllProperties();
    RentalsResponse rentalsResponse = new RentalsResponse();
    rentalsResponse.setRentals(propertyDtoList);
    return ResponseEntity.ok(rentalsResponse);
  }

  @GetMapping("/name/{name}")
  public ResponseEntity<PropertyDto> getPropertyByName(@PathVariable String name){
    Optional<PropertyDto> propertyDtoOptional = propertyService.getPropertyByName(name);
    return propertyDtoOptional.map(propertyDto -> new ResponseEntity<>(propertyDto, HttpStatus.OK))
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping
  public ResponseEntity<PropertyDto> createProperty(@RequestPart("picture") MultipartFile picture,
                                                    @RequestParam("name") String name,
                                                    @RequestParam("surface") Integer surface,
                                                    @RequestParam("price") Integer price,
                                                    @RequestParam("description") String description,
                                                    Authentication authentication) {

    String currentUserEmail = authentication.getName();
    Optional<UserDto> userDtoOptional = this.userService.getUserFromEmail(currentUserEmail);
    Integer userId = userDtoOptional.map(UserDto::getId).orElse(null);

    PropertyDto createdProperty = new PropertyDto();
    createdProperty.setName(name);
    createdProperty.setDescription(description);
    createdProperty.setSurface(surface);
    createdProperty.setPicture(picture.getOriginalFilename());
    createdProperty.setPrice(price);
    createdProperty.setCreated_at(LocalDateTime.now());
    createdProperty.setUpdated_at(LocalDateTime.now());

    try {
      imageService.saveFileToExternalFolder(picture);
      propertyService.createProperty(createdProperty, userId);
      return new ResponseEntity<>(createdProperty, HttpStatus.CREATED);
    } catch (IOException e) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PutMapping("/{id}")
  public ResponseEntity<PropertyDto> updateProperty(@PathVariable Integer id, @RequestBody PropertyDto updatedPropertyDto, Authentication authentication) {
    PropertyDto updatedProperty = propertyService.updateProperty(id, updatedPropertyDto);
    return updatedProperty != null
      ? ResponseEntity.ok(updatedProperty)
      : ResponseEntity.notFound().build();
  }

  @DeleteMapping("/delete/{propertyId}")
  public ResponseEntity<String> deleteProperty(@PathVariable Integer propertyId) {
    boolean deleted = propertyService.deleteProperty(propertyId);
    return (deleted)
      ? new ResponseEntity<>("Property deleted successfully", HttpStatus.OK)
      : new ResponseEntity<>("Property not found", HttpStatus.NOT_FOUND);
  }

}
