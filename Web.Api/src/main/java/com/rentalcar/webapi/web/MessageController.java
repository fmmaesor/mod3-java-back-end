package com.rentalcar.webapi.web;

import com.rentalcar.webapi.dto.MessageDto;
import com.rentalcar.webapi.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/messages")
public class MessageController {

  private final MessageService messageService;

  @Autowired
  public MessageController(MessageService messageService) {
    this.messageService = messageService;
  }

  @GetMapping("/{messageId}")
  public ResponseEntity<MessageDto> getMessage(@PathVariable Integer messageId) {
    MessageDto messageDto = messageService.getMessageById(messageId);
    return ResponseEntity.of(Optional.ofNullable(messageDto));
  }

  @PostMapping
  public ResponseEntity<MessageDto> createMessage(@RequestBody MessageDto messageDto) {
    MessageDto createdMessage = messageService.createMessage(messageDto);
    return ResponseEntity.ok(createdMessage);
  }

  @PutMapping("/{messageId}")
  public ResponseEntity<MessageDto> updateMessage(@PathVariable Integer messageId, @RequestBody String newMessage) {
    Optional<MessageDto> updatedMessage = messageService.updateMessage(messageId, newMessage);
    return updatedMessage.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
  }

  @DeleteMapping("/{messageId}")
  public ResponseEntity<Void> deleteMessage(@PathVariable Integer messageId) {
    boolean deleted = messageService.deleteMessage(messageId);
    return deleted ? ResponseEntity.noContent().build() : ResponseEntity.notFound().build();
  }
}
