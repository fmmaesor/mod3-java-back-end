package com.rentalcar.webapi.web;

import com.rentalcar.webapi.dto.UserDto;
import com.rentalcar.webapi.dto.UserFormDataDto;
import com.rentalcar.webapi.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserController {

  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/{id}")
  public ResponseEntity<UserDto> getUserById(@PathVariable Integer id) {
    UserDto userDto = userService.getFromId(id);
    return (userDto != null)
      ? new ResponseEntity<>(userDto, HttpStatus.OK)
      : new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping("/name/{name}")
  public ResponseEntity<UserDto> getUserByName(@PathVariable String name) {
    Optional<UserDto> userDtoOptional = userService.getUserFromName(name);
    return userDtoOptional.map(userDto -> new ResponseEntity<>(userDto, HttpStatus.OK))
      .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PutMapping("/update/{userId}")
  public ResponseEntity<UserDto> updateUser(@PathVariable Integer userId, @RequestBody UserFormDataDto userFormDataDto) {
    Optional<UserDto> updatedUser = userService.updateUser(userId, userFormDataDto.getName(), userFormDataDto.getPassword(), userFormDataDto.getEmail());
    return updatedUser.map(userDto -> new ResponseEntity<>(userDto, HttpStatus.OK))
      .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @DeleteMapping("/delete/{userId}")
  public ResponseEntity<String> deleteUser(@PathVariable Integer userId) {
    boolean deleted = userService.deleteUser(userId);
    return (deleted)
      ? new ResponseEntity<>("User deleted successfully", HttpStatus.OK)
      : new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
  }
}
