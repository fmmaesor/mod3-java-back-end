package com.rentalcar.webapi.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

  @Value("${external.images.folder}")
  private String externalImagesFolder;

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    // Map requests for /image-name to your image directory
    registry.addResourceHandler("/**")
      .addResourceLocations("file:" + externalImagesFolder + "/"); // Replace with your actual path
  }
}
