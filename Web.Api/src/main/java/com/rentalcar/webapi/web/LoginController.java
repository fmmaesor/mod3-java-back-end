package com.rentalcar.webapi.web;

import com.rentalcar.webapi.domain.User;
import com.rentalcar.webapi.dto.UserFormDataDto;
import com.rentalcar.webapi.dto.UserDto;
import com.rentalcar.webapi.mapper.UserMapper;
import com.rentalcar.webapi.security.JwtTokenProvider;
import com.rentalcar.webapi.security.utils.JwtResponse;
import com.rentalcar.webapi.security.utils.LoginRequest;

import com.rentalcar.webapi.services.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class LoginController {


  private final AuthenticationManager authenticationManager;
  private final JwtTokenProvider jwtTokenProvider;

  private final UserMapper userMapper;
  private final UserService userService;

  public LoginController(AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider, UserMapper userMapper, UserService userService) {
    this.authenticationManager = authenticationManager;
    this.jwtTokenProvider = jwtTokenProvider;
    this.userMapper = userMapper;
    this.userService = userService;
  }

  @PostMapping("/login")
  public ResponseEntity<JwtResponse> login(@RequestBody final LoginRequest login) {
    final UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
      login.getEmail(), login.getPassword());

    final Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
    final SecurityContext securityContext = SecurityContextHolder.getContext();
    securityContext.setAuthentication(authentication);
    final String jwt =  this.jwtTokenProvider.generateJwtToken(authentication);
    final HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.add("Authorization", "Bearer " + jwt);
    return new ResponseEntity<>(new JwtResponse(jwt), httpHeaders, HttpStatus.OK);
  }

  @GetMapping("/me")
  public ResponseEntity<UserDto> getCurrentUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    if (authentication != null && authentication.getPrincipal() instanceof User) {
      User currentUser = (User) authentication.getPrincipal();

      // Convert User entity to UserDto
      UserDto userDto = this.userMapper.toDto(currentUser);

      return ResponseEntity.ok(userDto);
    }

    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
  }

  @PostMapping("/register")
  public ResponseEntity<UserDto> createUser(@RequestBody final UserFormDataDto user) {
    UserDto createdUser = userService.createUser(user);
    return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
  }
}
